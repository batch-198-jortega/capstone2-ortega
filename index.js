const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4000;

// Establish Connection to database
mongoose.connect("mongodb+srv://admin:admin123@cluster0.cyhnqy7.mongodb.net/eCommerceAPI?retryWrites=true&w=majority",{

	useNewUrlParser: true,
	useUnifiedTopology: true

});

// Check if successfully connected
let db = mongoose.connection;

db.on('error',console.error.bind(console, "MongoDB Connection Error."));
db.once('open',() => console.log("Successfully Connected to MongoDB."));

app.use(express.json());

// import User Routes
const userRoutes = require("./routes/userRoutes");
app.use('/users',userRoutes); 

// import Product Routes
const productRoutes = require("./routes/productRoutes");
app.use('/products',productRoutes);



app.listen(port,() => console.log(`Server is running at localhost:${port}`));