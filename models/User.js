// E-Commerce API User Model
const mongoose = require("mongoose");

// User Schema Model
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true,"First name is required"]
	},
	lastName: {
		type: String,
		required: [true,"Last name is required"]
	},
	email: {
		type: String,
		required: [true,"Email is required"],
	},
	password: {
		type: String,
		required: [true,"Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile No. is required"],
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [

		{
			totalAmount: {
				type: Number
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			products: [

				{
					productId: {
						type: String
					},
					quantity: {
						type: Number
					}
				}

			]
		}

	],
	accountCreated: {
		type: Date,
		default: new Date()
	}

});


module.exports = mongoose.model("User",userSchema);