const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true,"Product Name is required."]
	},
	description: {
		type: String,
		required: [true,"Product Descsription is required."]
	},
	price: {
		type: Number,
		required: [true,"Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	dateListed: {
		type: Date,
		default: new Date()
	},
	orders: [

		{
			orderId: {
				type: String
			},
			userId: {
				type: String
			},
			quantity: {
				type: Number
			},
			purchaseDate: {
				type: Date,
				default: new Date()
			}
		}

	]	

});

module.exports = mongoose.model("Product",productSchema);