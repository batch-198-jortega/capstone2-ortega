const express = require("express");
const router = express.Router();

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

const userControllers = require("../controllers/userControllers");

// Register New User
router.post('/register',userControllers.registerUser);

// Login User
router.post('/login',userControllers.loginUser);

// User Details
router.get('/details',verify,userControllers.getUserDetails);

// User Create Order
router.post('/checkOut',verify,userControllers.checkOutOrder);

// Set User as admin
router.put('/updateAdmin/:userId',verify,verifyAdmin,userControllers.setUserAsAdmin);

// get User's Order History
router.get('/orderHistory',verify,userControllers.orderHistory);

// get Orders from Users
router.get('/orders',verify,verifyAdmin,userControllers.getAllOrders);

// get User's Specific Order
router.get('/orderHistory/:orderId',verify,userControllers.getOneOrderHistory);


module.exports = router;