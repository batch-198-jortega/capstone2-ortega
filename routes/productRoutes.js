const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// Show all active products. Accessible by All Users.
router.get('/activeProducts',productControllers.getActiveProducts);

// Search for One Product. Accessible by All Users.
router.get('/singleProduct/:productId',productControllers.getSingleProduct);

// Create a product. Accessible by Admin Only
router.post('/addProduct',verify,verifyAdmin,productControllers.addProduct);

// Update a product. Accessible by Admin Only
router.put('/updateProduct/:productId',verify,verifyAdmin,productControllers.updateProduct);

// Archive a product. Accessible by Admin Only
router.delete('/archiveProduct/:productId',verify,verifyAdmin,productControllers.archiveProduct);

// Reactivate a product. Accessible by Admin Only
router.put('/activateProduct/:productId',verify,verifyAdmin,productControllers.activateProduct);

// Get all Products. Accessible by Admin Only
router.get('/allProducts',verify,verifyAdmin,productControllers.getAllProducts);

// Apply Sale to multiple Products, Accessible by Admin Only
router.put('/applySale',verify,verifyAdmin,productControllers.applySale);


module.exports = router;