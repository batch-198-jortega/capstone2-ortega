const User = require("../models/User");
// Import User Model

const Product = require("../models/Product");
// Import Product Model

const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (req,res) => {

	// console.log(req.body); check if data is being received

	User.findOne({email:req.body.email})
	.then(doesUserExist => {

		// console.log(doesUserExist); check if value being returned is null

		if(doesUserExist === null){
			const hashedPassword = bcrypt.hashSync(req.body.password,10);

			let newUser = new User({

				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				password: hashedPassword,
				mobileNo: req.body.mobileNo

			});

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));

		} else {

			res.send({message: "User Already Exists"});

		};

	});

};

module.exports.loginUser = (req,res) => {

	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null) {

			// if User entered an invalid email. Send Message.
			return res.send({message: "User does not exist."});

		} else {

			const passwordMatch = bcrypt.compareSync(req.body.password,foundUser.password);

			if(passwordMatch){

				return res.send({accessToken: auth.createAccessToken(foundUser)});

			} else {

				// If user entered the wrong Password. Send Message.
				return res.send({message: "Password does not match!"});

			};
		};
	});
};

module.exports.getUserDetails = (req,res) => {

	User.findById(req.user.id)
	.then(user => {

		if(user.isAdmin){

			// Show only USER DETAILS that is relevant to respective USER
			const {orders,...userDetails} = user.toObject();

			res.send(userDetails);

		} else {

			// Show only USER DETAILS that is relevant to respective USER
			const {orders,isAdmin,...userDetails} = user.toObject();

			res.send(userDetails);

		};

	})
	.catch(error => res.send(error));

};

module.exports.checkOutOrder = async (req,res) => {

	// console.log(req.user.id); // check user Id
	// console.log(req.body); // check orders and quantity

/*	req.body.products.forEach((k) =>{

		// console.log(k);

	})*/

	if(req.user.isAdmin){

		return res.send({message: "Admin is not allowed to buy own products."});

	}

	let isUserUpdated = await User.findById(req.user.id).then(async (user) => {

		// console.log(user.orders);

		let index = (user.orders.length === 0)? 0 : (user.orders.length);
		// console.log(index);

		// Compute Total Amount of products purchased PER ORDER
		let amount = 0; // reset amount to 0 for every ORDER
		let totalAmount = {
			totalAmount: amount
		};

		user.orders.push(totalAmount);

		let product = req.body.products.map((p) => {
			return p;
		});
		// console.log(product);

		for(let i = req.body.products.length; i>0; i--){
			let isAmountUpdated = await Product.findById(product[i-1].productId).then(result => {

				amount += result.price * req.body.products[i-1].quantity;

				user.orders[index].totalAmount = amount;

				return user.save().then(user => true).catch(err => err.message);
			});

			// console.log(isAmountUpdated); if isAmountUpdated returns false, send the error to client and stop the process
			if(isAmountUpdated !== true){
				return res.send({message: isAmountUpdated});
			};

		};

		// Add product in the array in users orders proper index
		req.body.products.forEach((product) => {

			let newProduct = {

				productId: product.productId,
				quantity: product.quantity

			};

			user.orders[index].products.push(newProduct);

		});

		// console.log(user.orders[index]);
		return user.save().then(user => true).catch(err => err.message);

	});

	// console.log(isUserUpdated); if isUserUpdated returns false, send the error to client and stop the process
	if(isUserUpdated !== true){
		return res.send({message:isUserUpdated});
	};	

	// else, continue and update products

	req.body.products.forEach(async (item) => {
		let isProductUpdated = await Product.findById(item.productId).then(product => {

			let buyer = {
				userId: req.user.id,
				quantity: item.quantity,
			};

			product.orders.push(buyer);

			return product.save().then(product => true).catch(err => err.message);
		});

		// console.log(isProductUpdated); // if PRODUCT is not updated, send the error to client and stop the process
		if(isProductUpdated !== true){
			return res.send({message: isProductUpdated});
		};

	});

	User.findById(req.user.id).then(user => {

		let index = user.orders.length - 1;
		return res.send({
			message: "Order Successful! Thank you for purchasing!",
			totalAmount: `Php ${user.orders[index].totalAmount}`,
			orders: user.orders[index]	
		});
	});
};

module.exports.setUserAsAdmin = (req,res) => {

	// console.log(req.params.userId); // Check

	let setUserAsAdmin = {

		isAdmin: true
	};

	User.findByIdAndUpdate(req.params.userId,setUserAsAdmin,{new:true})
	.then(newAdmin => {

		let {orders,password,...details} = newAdmin.toObject();
		res.send(details)

	})
	.catch(error => res.send(error));

};

module.exports.orderHistory = (req,res) => {

	// console.log(req.user.id); // Check

	User.findById(req.user.id)
	.then(user => {

		if(user.orders.length === 0){

			res.send({message: "You haven't purchased anything yet. Buy Now!"})

		} else {

			res.send(user.orders);

		};

	})
	.catch(error => res.send(error));

};

module.exports.getAllOrders = (req,res) => {

	User.find()
	.then(users => {

		let allOrders = users.map((user) => {

			const container = {};

			if(user.orders.length > 0){

				container.userId = user._id;
				container.orders = user.orders;
				return container;

			};

		});

		let filteredOrders = allOrders.filter((x) => {
			return x != null;
		});

		// console.log(filteredOrders);
		res.send(filteredOrders);

	})
	.catch(error => res.send(error));

};

module.exports.getOneOrderHistory = (req,res) => {

	// console.log(req.user.id);
	// console.log(req.params.orderId);

	let userOrderHistory = User.findById(req.user.id).then(user => {

		let index = user.orders.length;
		// console.log(index);
		for(index; index > 0; index--){

			if(user.orders[index-1].id === req.params.orderId){

				// console.log(user.orders[index-1]);
				res.send(user.orders[index-1]);

			} else {

				continue;

			};

		};

	}).catch(error => res.send(error));

};