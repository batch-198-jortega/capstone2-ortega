const mongoose = require("mongoose");

const Product = require("../models/Product");

const auth = require("../auth");

module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive:true})
	.then(result => {

		let allActiveProducts = result.map((product) => {

			const activeProducts = {};

			activeProducts.id = product._id;
			activeProducts.name = product.name;
			activeProducts.description = product.description;
			activeProducts.price = product.price;
			activeProducts.dateListed = product.dateListed;

			return activeProducts;

		});

		res.send(allActiveProducts);
	})
	.catch(error => res.send(error));

};

// Find product by product ID
module.exports.getSingleProduct = (req,res) => {

	// console.log(req.params.productId);
	const isIdValid = mongoose.Types.ObjectId.isValid(req.params.productId);
	// console.log(isIdValid);

	// Check if provided ID is Valid. Else, Send Message that ID does not exist or ID provided is Invalid
	if (isIdValid) {

		Product.findById(req.params.productId)
		.then(product => {

			// console.log(product);
			if (product === null) {

				// If ID is valid but product does not exist. Show Message
				res.send({message: "No results found."});

			} else if (product.isActive) {

				// show only relevant product Details. Hide Order Information in Product Details.
				const {orders,isActive,...productDetails} = product.toObject();

				res.send(productDetails);

			} else {

				// product is inActive, show message.
				res.send({message: "Product is unavailable"});

			};

		})
		.catch(error => res.send(error));

	} else {

		res.send({message: "Product ID does not exist or is invalid"});

	};

};

// Create a listing of a Product
module.exports.addProduct = (req,res) => {


	Product.findOne({name:req.body.name})
	.then(product => {

		// Check if product already exists.
		if(product === null){

			let newProduct = new Product ({

				name: req.body.name,
				description: req.body.description,
				price: req.body.price

			});

			newProduct.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));

		} else {

			res.send({message: "Product already exists."});

		};
	});

};

// Update or Edit a Product Information: limited to NAME, DESCRIPTION and PRICE ONLY.
module.exports.updateProduct = (req,res) => {

	// console.log(req.params.productId);

	// console.log(req.body);

	let updatedProduct = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	};

	Product.findByIdAndUpdate(req.params.productId,updatedProduct,{new:true})
	.then(product => {

		// Check first if product exists before updating
		if(product === null){

			res.send({message: "Product does not exist."});

		} else {

			res.send(product);

		};
	})
	.catch(error => res.send(error));

};

module.exports.archiveProduct = (req,res) => {

	// console.log(req.params.productId);

	let delistProduct = {

		isActive: false

	};

	Product.findById(req.params.productId)
	.then(result => {

		// Check if product is Active or not before Updating and inform Admin
		if(result.isActive){

			Product.findByIdAndUpdate(result,delistProduct,{new:true})
			.then(product => res.send({

				message: "Delisting Successful",
				product

			}))
			.catch(error => res.send(error));

		} else {

			res.send({message: "Product is already inactive"});

		};
	});
};

module.exports.activateProduct = (req,res) => {

	// console.log(req.params.productId);

	let activateProduct = {

		isActive: true

	};

	Product.findById(req.params.productId)
	.then(result => {

		// Check if product is Active or not before Updating and inform Admin
		if(result.isActive){

			res.send({message: "Product is already active"});

		} else {

			Product.findByIdAndUpdate(result,activateProduct,{new:true})
			.then(product => res.send({

				message: "listing Successful",
				product

			}))
			.catch(error => res.send(error));

		};
	});
};

module.exports.getAllProducts = (req,res) => {

	Product.find()
	.then(products => res.send(products))
	.catch(error => res.send(error));

};

module.exports.applySale = (req,res) => {

	// console.log(req.user.id);
	// console.log(req.body);

	let requestSale = req.body;
	console.log(requestSale);

	// let a be the groups of product per value of sale or batch
	req.body.forEach((a) => {

		// console.log(a.sale);
		// console.log(a.products);

		a.products.forEach(async (productsOnSale) => {

			let isProductOnSale = await Product.findById(productsOnSale.productId)
			.then(product => {

				// To save previous price for checking
				// let originalPrice = product.price
				// console.log(originalPrice);

				// Save Sale Price
				let salePrice = product.price * ((100-a.sale)/100);

				product.price = Math.ceil(salePrice);

				// To check in console if success
				// let {orders,...details} = product.toObject();
				// console.log(details);

				// return true
				return product.save().then(product => true).catch(err => err.message);
			});

			// if isProductOnSale returns false, send the error to client and stop the process.
			console.log(isProductOnSale);
			if(isProductOnSale !== true){

				return res.send({message: isProductOnSale});

			};
		});		
	});

	res.send({

		message: "Discounts on the following products Successful!",
		requestSale

	});

};



